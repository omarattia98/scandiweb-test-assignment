<?php

namespace Classes;

use stdClass;

abstract class Product{

    protected $id;
    protected $SKU;
    protected $name;
    protected $price;
    protected $type;
    protected $attribute;



    public function set($values){
        if(isset($values->id)){
            $this->id = $values->id;
        }
        $this->SKU = $values->SKU;
        $this->name = $values->name;
        $this->price = json_decode($values->price);
        $this->type = $values->type;
    }
    public function get(){
        $retObject = array(
            "id" => $this->id,
            "SKU" => $this->SKU,
            "name" => $this->name,
            "price" => $this->price,
            "type" => $this->type,
            "attribute" => clone $this->attribute,
        );
        return $retObject;
    }

    public function InsertDB(){
        $sql = "INSERT INTO Products
        (SKU, name, price, type, height, width, length, size, weight)
        VALUES
            ('".$this->SKU."', '".$this->name."', ".$this->price.", '".$this->type."', NULLIF('".$this->attribute->height."',''),  NULLIF('".$this->attribute->width."',''),  NULLIF('".$this->attribute->length."',''),  NULLIF('".$this->attribute->size."',''),  NULLIF('".$this->attribute->weight."',''));";
        return $sql;
    }

}