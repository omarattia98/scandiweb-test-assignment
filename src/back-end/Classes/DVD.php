<?php

namespace Classes;

class DVD extends Product{

    public function __construct() {
        $this->attribute = (object) array(
            'size' => null,
        );
    }
    public function set($values){
        parent::set($values);
        $att = json_decode($values->attribute);
        $this->attribute->size = $att->{'size'};
    }

}