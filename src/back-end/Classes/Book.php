<?php

namespace Classes;

class Book extends Product{

    public function __construct() {
        $this->attribute = (object) array(
            'weight' => null,
        );
    }
    public function set($values){
        parent::set($values);
        $att = json_decode($values->attribute);
        $this->attribute->weight = $att->{'weight'};
    }

}