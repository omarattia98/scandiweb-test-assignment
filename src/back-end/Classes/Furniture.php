<?php

namespace Classes;

class Furniture extends Product{

    public function __construct() {
        $this->attribute = (object) array(
            'height' => null,
            'width' => null,
            'length' => null,
        );
    }
    public function set($values){
        parent::set($values);
        $att = json_decode($values->attribute);

        $this->attribute->height = $att->{'height'};
        $this->attribute->width = $att->{'width'};
        $this->attribute->length = $att->{'length'};
    }

}