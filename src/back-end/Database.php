<?php

class Database
{
    private static $conn = null;

    private function __construct()
    {
        $host = '178.79.135.172:34066';
        $dbName =  'Scandiweb';
        $user =  'root';
        $password = 'helloOmar123Buddy';

        try
        {
            $dbURI = 'mysql:host=' . $host . ';port=34066;dbname=' . $dbName;
            $conn = new PDO($dbURI, $user, $password);
            $conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::$conn = $conn;
        }
        catch(PDOException $e)
        {
            echo "Connection failed: " . $e->getMessage();
            exit;
        }
    }

    public static function getConnection()
    {
        if(!self::$conn){
            new Database();
        }
        return self::$conn;
    }
}
