<?php

include_once 'AutoLoader.php';

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header("Access-Control-Allow-Methods: GET,POST,DELETE");
header("Access-Control-Allow-Headers: Accept,Content-Type,Cache-Control");

$conn =  Database::getConnection();
if (isset($_GET['method'])) {
    $_SERVER['REQUEST_METHOD'] = $_GET['method'];
}
$request = $_SERVER["REQUEST_METHOD"];


$requestHandler = (new Handlers\Controller($request));

echo json_encode($requestHandler->request());