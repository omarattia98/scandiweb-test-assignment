import React from 'react';
import { getIn } from 'formik';

function ProductDVDForm ({formik}) {

    return (
        <div>
            <div className='row justify-content-start'>
                <label className='col-md-2' >Size (MB)</label>
                <input
                className='col-md-4'
                id='size'
                name='attribute.size'
                placeholder='Please, provide size'
                type="number"
                value={formik.values.attribute.size}
                onChange={formik.handleChange}
                />
                {(getIn(formik.touched,"attribute.size") && getIn(formik.errors,"attribute.size")) &&
                <div className='col-md-5 text-danger'>{getIn(formik.errors,"attribute.size")}</div>}
                                <p className='row my-4'>Please, provide Weight in MB</p>

            </div>

        </div>
    );
};

export default ProductDVDForm;