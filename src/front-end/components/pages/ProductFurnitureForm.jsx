import React from 'react';
import { getIn } from 'formik';

function ProductFurnitureForm ({formik}) {


    return (
        <div>
            <div className='row justify-content-start'>
                <label className='col-md-2' >Height (CM)</label>
                <input
                className='col-md-4'
                id='height'
                name='attribute.height'
                placeholder='Please, provide height'
                type="number"
                value={formik.values.attribute.height}
                onChange={formik.handleChange}
                />
                {(getIn(formik.touched,"attribute.height") && getIn(formik.errors,"attribute.height")) &&
                <div className='col-md-5 text-danger'>{getIn(formik.errors,"attribute.height")}</div>}
            </div>
            <div className='row my-4'>
                <label className='col-md-2' >Width (CM)</label>
                <input
                className='col-md-4'
                id='width'
                name='attribute.width'
                placeholder='Please, provide width'
                type="number"
                value={formik.values.attribute.width}
                onChange={formik.handleChange}
                />
                {(getIn(formik.touched,"attribute.width") && getIn(formik.errors,"attribute.width")) &&
                <div className='col-md-5 text-danger'>{getIn(formik.errors,"attribute.width")}</div>}
            </div>
            <div className='row my-4'>
                <label className='col-md-2' >Length (CM)</label>
                <input
                className='col-md-4'
                id='length'
                placeholder='Please, provide length'
                name='attribute.length'
                type="number"
                value={formik.values.attribute.length}
                onChange={formik.handleChange}
                />
                {(getIn(formik.touched,"attribute.length") && getIn(formik.errors,"attribute.length")) &&
                <div className='col-md-5 text-danger'>{getIn(formik.errors,"attribute.length")}</div>}
            </div>
            <p className='row my-4'>Please, provide Dimensions in HxWxL</p>
        </div>
    );
};

export default ProductFurnitureForm;