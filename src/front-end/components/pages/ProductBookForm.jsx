import React from 'react';
import { getIn } from 'formik';

function ProductBookForm ({formik}) {


    return (
        <div>
            <div className='row justify-content-start'>
                <label className='col-md-2' >Weight (KG)</label>
                <input
                className='col-md-4'
                id='weight'
                name='attribute.weight'
                placeholder='Please, provide weight'
                type="number"
                value={formik.values.attribute.weight}
                onChange={formik.handleChange}
                />
                {(getIn(formik.touched,"attribute.weight") && getIn(formik.errors,"attribute.weight")) &&
                <div className='col-md-5 text-danger'>{getIn(formik.errors,"attribute.weight")}</div>}
                <p className='row my-4'>Please, provide Weight in KG</p>

            </div>
        </div>
    );
};

export default ProductBookForm;