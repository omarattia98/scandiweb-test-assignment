import React, { useEffect } from 'react';
import useState from 'react-usestateref'
import getAllProducts, { removeProducts } from '../../api/api';
import Product from '../Product';
import { Link } from "react-router-dom";;

function ProductListPage() {


  const [itemList, setItemList, itemref] = useState([])
  const [selectedItems, setSelectedItems] = useState([])

  const APIGetItems = () => {
    getAllProducts(setItemList);
  }

  const HandleItemSelect = (key) => {

    if (selectedItems.includes(key)) {
      const correct = [...selectedItems];
      correct.splice(correct.indexOf(key), 1);
      setSelectedItems(correct);

    } else {
      setSelectedItems([...selectedItems, key]);
    }
  }

  const onClickMassDelete = () => {
      removeProducts(selectedItems, setItemList)
  }


  const listItems = itemref.current?.map((item) =>
    <div key={item.id} className='my-2 mx-3 productCard'>
      <Product key={item.id} productData={item} onSelectHandle={() => HandleItemSelect(item.id)} />
    </div>
  );

  useEffect(() => {
    APIGetItems()
  }, [listItems.length])

  return (
    <div>
      <div className="header">
        <div className='d-flex flex-row justify-content-between'>
          <h1>
            Product list
          </h1>
          <div>
            <Link to="/add-product">
              <button id='add-product-btn' className='btn btn-primary btn-lg  mx-2'>ADD</button>
            </Link>
            <button id='delete-product-btn' /* disabled={selectedItems.length === 0 ? true : false} */ className='btn btn-danger btn-lg ' onClick={onClickMassDelete}>MASS DELETE</button>
          </div>
        </div>
        <hr />
      </div>

      <div className='d-flex flex-row flex-wrap'>
        {listItems}
      </div>
    </div>
  );
}

export default ProductListPage;