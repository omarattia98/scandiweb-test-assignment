import React from 'react';
import { useFormik } from 'formik';
import { useNavigate } from 'react-router-dom';
import * as yup from 'yup';
import { addNewProduct } from '../../api/api';
import { ProductTypeFormMap, ProductTypes } from '../ProductTypes';

function AddProductPage() {
  const navigate = useNavigate()

  const formik = useFormik({
    initialValues: {
      SKU: '',
      name: '',
      price: '',
      type: '',
      attribute: {
        size: '',
        weight: '',
        length: '',
        width: '',
        height: '',
      },
    },
    validationSchema: yup.object().shape({
      SKU: yup.string().required('SKU required'),
      name: yup.string().required('Name required'),
      price: yup.number().min(0, ()=> "Please, provide the data correctly").required('Price required'),
      type: yup.string().required('Type required'),
      attribute: yup.object().when(
        "type",
        {
          is: ProductTypes.FURNITURE,
          then: (schema) => schema.shape({
            length: yup.number().min(0, ()=> "Please, provide the data correctly").required("Length required"),
            width: yup.number().min(0, ()=> "Please, provide the data correctly").required("Width required"),
            height: yup.number().min(0, ()=> "Please, provide the data correctly").required("Height required"),
          })
        })
        .when(
        "type",
        {
          is: ProductTypes.BOOK,
          then: (schema) => schema.shape({
            weight: yup.number().min(0, ()=> "Please, provide the data correctly").required("Weight required"),
          })
        })
        .when(
        "type",
        {
          is: ProductTypes.DVD,
          then: (schema) => schema.shape({
            size: yup.number().min(0, ()=> "Please, provide the data correctly").required("Size required"),
          })
        }),
      }),
    onSubmit: values => {
      addNewProduct(values)
      formik.resetForm()
      navigate('/')
    },
  })

  const onClickCancel = () => {
    formik.resetForm()
    navigate('/')
  }

  const Options = Object.keys(ProductTypes).map(key =>{
    return (<option key={ProductTypes[key]} value={ProductTypes[key]}>{ProductTypes[key]}</option>)
  })
  const AttributeForm = ProductTypeFormMap[formik.values.type || "None"]

  return (
    <div>
      <form onSubmit={formik.handleSubmit} id="product_form">
        <div className="header">
        <div className='d-flex flex-row justify-content-between'>
          <h1>
              Add a Product
            </h1>
            <div>
              <button type="submit" id='save-product-btn' className='btn btn-primary btn-lg'>Save</button>
              <button type="button" id='cancel-product-btn' className='btn btn-danger mx-2 btn-lg' onClick={onClickCancel}>Cancel</button>
            </div>
          </div>
          
          <hr/>
        </div>

        <div className='container-fluid px-3'>
          <div className='row my-4 form-control-lg'>
            <label className='col-md-2 form-label'>SKU</label>
            <input
              className='col-md-4'
              id='sku'
              name='SKU'
              type="text"
              placeholder='Enter SKU...'
              value={formik.values.SKU}
              onChange={formik.handleChange}
            />{(formik.touched.SKU && formik.errors.SKU) &&
            <div className='col-md-4 text-danger'>{"Please, submit required data"}</div>}
          </div>
          <div className='row my-4 form-control-lg'>
            <label className='col-md-2'>Name</label>
            <input
              className='col-md-4'
              id='name'
              name='name'
              placeholder='Enter Name...'
              type="text"
              value={formik.values.name}
              onChange={formik.handleChange}
            />{(formik.errors.name && formik.touched.name) &&
              <div className='col-md-4 text-danger'>{"Please, submit required data"}</div>}
          </div>
          <div className='row my-4 form-control-lg'>
            <label className='col-md-2' >Price($) </label>
            <input
              className='col-md-4'
              id='price'
              name='price'
              placeholder='Enter Price...'
              type="number"
              value={formik.values.price}
              onChange={formik.handleChange}
            />{(formik.errors.price && formik.touched.price) &&
              <div className='col-md-4 text-danger'>{"Please, submit required data"}</div>}
          </div>
          <div className='row my-4 form-control-lg'>
            <label className='col-md-2' >Type Switcher</label>
            <select
              className='col-md-2'
              id="productType"
              name="type"
              value={formik.values.type}
              onChange={formik.handleChange}
            >
            <option value="" disabled>Product type</option>
            {
              Options
            }
            </select>{(formik.errors.type && formik.touched.type) &&
            <div className='col-md-4 text-danger'>{"Please, submit required data"}</div>}
          </div>
          <div className='form-control-lg'>
            <AttributeForm formik={formik}/>
          </div>
        </div>
      </form>
    </div>
  );
}

export default AddProductPage;