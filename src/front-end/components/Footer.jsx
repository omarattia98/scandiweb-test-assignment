import React from 'react';
import '../scss/Footer.scss';

function Footer () {
    return (
        <footer id="footer" className="mx-auto pb-3 mt-auto">
            <hr/>
            <div className='text-center'>
                <span>Scandiweb Test Assignment</span>
            </div>
        </footer>
    );
};

export default Footer;