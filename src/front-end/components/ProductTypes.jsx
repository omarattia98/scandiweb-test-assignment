import ProductDVDForm from "./pages/ProductDVDForm";
import ProductBookForm from "./pages/ProductBookForm";
import ProductFurnitureForm from "./pages/ProductFurnitureForm";

export const ProductTypeAttributeMap = {
    "DVD": ["Size: ","MB"],
    "Book": ["Weight: ","KG"],
    "Furniture": ["Dimension: ",""],

}

export const ProductTypeFormMap = {
    "DVD": ProductDVDForm,
    "Book": ProductBookForm,
    "Furniture": ProductFurnitureForm,
    "None": () => <div/>,
}

export const ProductTypes = {
    DVD: "DVD",
    BOOK: "Book",
    FURNITURE: "Furniture",
}