import React, { useState } from 'react';
import '../scss/Product.scss';
import { ProductTypeAttributeMap } from './ProductTypes';

function Product ({productData, onSelectHandle}) {
    const [selected] = useState(false);
    const [data] = useState(productData);
    
    return (
        <div className="card p-1 h-100 w-100">
            <input className='delete-checkbox m-1 form-check-input' type="checkbox" value={selected} onChange={onSelectHandle}/>
            <div className='card-body text-center justify-content-evenly'>
                <h5 className="card-text font-size-sm">{data.name}</h5>
                <hr/>
                <h6 className="card-text mw-100 mh-100  text-wrap font-size-sm" id="card-name-region">SKU: {data.SKU}</h6>
                <h6 className="card-text mw-100 mh-100 font-size-sm">Price: ${data.price.toFixed(2)} </h6>
                <h6 className="card-text mw-100 mh-100 font-size-sm">{ProductTypeAttributeMap[data.type][0]}{Object.values(data.attribute).join('x')}{ProductTypeAttributeMap[data.type][1]}</h6>

            </div>
        </div>
    );
};

export default Product;