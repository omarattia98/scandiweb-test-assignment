
const API = "https://omarattia.com/back-end/";

export default async function getAllProducts(Data) {
    fetch(
        API,
        {
            method: "GET",
        }
      )
      .then(response => response.json())
      .then(results => Data(results || []))
      .catch(err => console.error(err))
}

export function removeProducts(Ids, Data) {
    fetch(
        API,
        {
            method: "DELETE",
            body: JSON.stringify(Ids),
        }
      )
      .then(response => response.json())
      .catch(err => console.error(err))
      .then(getAllProducts(Data))
}

export function addNewProduct(Data) {
    fetch(
        API,
        {

            method: "POST",
            body: JSON.stringify(Data),
        }
      )
      .then(response => response.json())
      .catch(err => console.error(err))
}

