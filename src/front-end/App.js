import React from 'react';
import {
  BrowserRouter as Router,
  Routes,
  Route
} from "react-router-dom";
import ProductListPage from './components/pages/ProductListPage';
import AddProductPage from './components/pages/AddProductPage';
import './scss/App.scss';
import Footer from "./components/Footer";

function App() {
  return (
    <div className="main-container d-flex flex-column">
      <Router>
        <div className="container-fluid bg overflow-auto pt-4 mb-5">
          <Routes>
            <Route exact path="/" element={<ProductListPage/>} />
            <Route path="/add-product" element={<AddProductPage/>} />
            <Route path="*" element={<p>Page Not Found</p>}/>
          </Routes>
        </div>
      </Router>
      <div className='mt-auto'>
        <Footer/>
      </div>
    </div>
  );
}

export default App;
